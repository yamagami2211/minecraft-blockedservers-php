<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
<meta name="viewport" content="width=device-width,initial-scale=1.0" />
<title>blockedservers確認ツール</title>
</head>
<div class="container">
  <div class="page-header" id="banner">
    <div class="row">
              <div class="panel panel-default">
  <div class="panel-heading">blockedservers</div>
  <div class="panel-body">

<?php
$url1 = "https://mcapi.ca/blockedservers";
if(isset($_GET["sha1"])){
$server1 = file_get_contents($url1);
    $arr = json_decode($server1,true);
    $list = $arr["found"][$_GET["sha1"]]["ip"];

    echo '<p>エラー出たら 「sha1 が間違ってる」か「IP が存在しない」か「そもそもblockedserverに載っていない」為エラーが出てます。</p>';
    echo '<kbd>'.$_GET["sha1"].'</kbd> = <span class="label label-info">'.$list.'</span>';
} else {

  echo '<span class="label label-warning">sha1をGETする必要があります。</span>';
  echo '<p>例: 「blockedservers.php?sha1=c22efe4cf7fb319ca2387bbc930c1fdf77ab72fc」</p>';
  echo '<span class="label label-danger">「709a849e4e7a986c8165083dccb9857bcb04c67b」はIPが見つからないっぽいです。</span>';
  echo '<hr>';

  echo '<p>エラー出たら 「sha1 が間違ってる」か「IP が存在しない」か「そもそもblockedserverに載っていない」為エラーが出てます。</p>';
  echo '<p>また、検証はしてないですが API使い過ぎにより表示されない こともあるかもです。</p>';
  echo '<hr>';

  echo 'sha1は<a href="https://sessionserver.mojang.com/blockedservers">こちら</a>で見れます。';
  echo '<hr>';

  echo 'APIは https://mcapi.ca のを利用しています。';

}
?>
</div>
</div>
      </div></div>
      </div>
      </div>
</body>
</html>